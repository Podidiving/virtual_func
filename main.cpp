#include <functional>
#include <iostream>
#include <map>


#define VIRTUAL_CLASS(className) \
struct className {

#define END(className) };

#define DECLARE_METHOD(className, methodName, code) \
table[#methodName] = [&] {code};

#define VIRTUAL_CLASS_DERIVED(derivedClassName, baseClassName) \
struct derivedClassName : baseClassName{

#define DEFINE_TABLE std::map<std::string, std::function<void()> > table;

#define END_DERIVE(derivedClassName, baseClassName) };

#define CONSTRUCT_OPEN(className) className() {

#define CONSTRUCT_CLOSE(className) }

#define DERIVE_METHODS(derivedClassName, baseClassName)\
baseClassName baseObject = baseClassName();\
for ( auto it = baseObject.table.begin(); it != baseObject.table.end(); it++ ) {\
    table[it->first] = it->second; \
}\

#define VIRTUAL_CALL(ptr, methodName) ptr->table[#methodName]();

VIRTUAL_CLASS(Base)
    DEFINE_TABLE
    CONSTRUCT_OPEN(Base)
        DECLARE_METHOD(Base, Both, {std::cout << "This method is both for Base and Derived structs. Now in Base\n";})
        DECLARE_METHOD(Base, OnlyBase, {std::cout << "This method is only defined in Base struct\n";})
    CONSTRUCT_CLOSE(Base)

END(BASE)

VIRTUAL_CLASS_DERIVED(Derived, Base)

    CONSTRUCT_OPEN(Derived)
        DERIVE_METHODS(Derived, Base)
        DECLARE_METHOD(Base, Both, {std::cout << "This method is both for Base and Derived structs. Now in Derived\n";})
        DECLARE_METHOD(Base, OnlyDerived, {std::cout << "This method is only defined in Derived struct\n";})
    CONSTRUCT_CLOSE(Derived)

END_DERIVE(Derived, Base)

int main() {
    Base base = Base();
    Derived derived = Derived();
    Base* reallyDerived = reinterpret_cast<Base*>(&derived);

    VIRTUAL_CALL((&base), Both)
    VIRTUAL_CALL(reallyDerived, Both)
    VIRTUAL_CALL(reallyDerived, OnlyBase)
    VIRTUAL_CALL(reallyDerived, OnlyDerived)
//    VIRTUAL_CALL((&base), OnlyDerived); <- won't compile

    return 0;
}